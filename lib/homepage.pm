package homepage;
use Dancer ':syntax';

get '/' => sub {
    content_type "text/plain";
    local config->{layout} = "homepage-layout-AAABBBCCC";
    template 'index', {title => "HOMEPAGE EXAMPLE"};
};
## OUTPUT: AS
## <!DOCTYPE>
## <html>
##     <head>
##         ---- HOMEPAGE SEO TAGS HERE ----
##         <title>HOMEPAGE EXAMPLE</title>
##     </head>
##     <body>
## ~~~~~~~~~~~~~
## ------------------------------
## --- HOME PAGE CONTENT HERE ---
## ------------------------------
## 
## ~~~~~~~~~~~~~
##     </body>
## </html>
## 
true;
